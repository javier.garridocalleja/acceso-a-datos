// GESTIONAR DATOS ALUMNO:
// Nombre (40 caracteres)
// Edad 
// MAXIMO 10

// OPCION 1: CARGAR DATOS (DEL FICHERO)
// OPCION 2: ESCRIBIR DATOS (EN EL FICHERO)
// OPCION 3: INSERTAR UN DATO EN LA TABLA
// OPCION 4: ESCRIBIR TABLA EN LA PANTALLA
// OPCION 5: BORRAR UN DATO DE LA TABLA (OPCIONAL)
// OPCION 6: SALIR

#include <stdio.h>
#include <stdlib.h>



/* --------------------------------------- */
/* Declaracion de la estructura de alumnos */
/* --------------------------------------- */
struct TAlumno
{	
	char nombre[40];
	int edad;
}TablaAlumno;

/* --------------------------------------- */
/*               FUNCIONES				   */
/* --------------------------------------- */
 
void cargardatos (FILE *pf, struct TAlumno TablaAlumno[10]) {
	struct TAlumno;
        char buffer[100];

        fscanf(pf, "%s", buffer);
        printf("%s", buffer);
}

void escribirdatos(FILE *pf, struct TAlumno TablaAlumno[10]) {
      struct TAlumno;
      int palabra=0;

      while ( (palabra = getc(pf)) !=EOF){
        printf("Introduce el nombre: ");
        scanf("%c", TablaAlumno->nombre);

        printf("Introduce la edad: ");
        scanf("%i", &TablaAlumno->edad);

        palabra++;
      }
      fwrite(TablaAlumno, sizeof(TablaAlumno), 1, pf);
}
void insertardatos(FILE *pf, struct TAlumno TablaAlumno[10]) {
      struct TAlumno;
      char buffer[100];

      fprintf(pf, buffer);
      fprintf(pf, "%s", "\nJavi.");


}
void escribirtabla() {
}
void borrardato(){
}

void menu() {
  printf("\t\tIntroduce una opcion\n");
  printf("\t1. Cargar datos en el fichero\n");
  printf("\t2. Escribir datos en el fichero\n");
  printf("\t3. Insertar un dato en la tabla\n");
  printf("\t4. Escribir tabla en la pantalla\n");
  printf("\t5. Borrar un dato en la tabla\n");
  printf("\t6. Salir\n");
}
/* --------------------------------------- */
/*          PROGRAMA PRINCIPAL			   */
/* --------------------------------------- */

int main() {
    int opcion;
    FILE *pf;
    struct TAlumno* TablaAlumno;
    if (!(pf = fopen("fichero.txt", "rt"))) {
        fprintf(stderr, "No se ha podido leer el fichero");
        return EXIT_FAILURE;
    }
    do {
        menu();
        scanf("%i",&opcion);

        switch(opcion) {
            case 1: cargardatos(pf, TablaAlumno);
                    printf("Cargando datos .... \n");
                    break;
            case 2: escribirdatos(pf, TablaAlumno);
                    printf("Escribiendo datos ... \n");
                    break;
            case 3: insertardatos(pf, TablaAlumno);
                    printf("Insertando datos ... \n");
                    break;
            case 4: escribirtabla();
                    printf("Escribiendo tabla ... \n");
                    break;
            case 5: borrardato();
                    printf("Borrando datos .... \n");
                    system("clear");
                    break;
            case 6: 
                    printf("Saliendo del programa.... \n");
                    break;
            default:
                    printf("\tIntroduce una opcion valida[1-6]\n\n");
                    break;
        }

    } while (opcion!=6);


    return 0;
}
